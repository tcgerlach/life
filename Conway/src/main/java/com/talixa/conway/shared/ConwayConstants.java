package com.talixa.conway.shared;

public class ConwayConstants {
	public static final String VERSION = "1.0";
	
	public static final String TITLE_MAIN = "Conway's Game of Life";	
	public static final String TITLE_ABOUT = "About " + TITLE_MAIN;
	
	public static final int APP_WIDTH = 510;
	public static final int APP_HEIGHT = 550;
	
	public static final int BORDER = 10;
	public static final int BORDER_SMALL = 5;
	
	public static final String LABEL_OK = "Ok";	
	
	public static final String MENU_FILE = "File";	
	public static final String MENU_EXIT = "Exit";
	public static final String MENU_SETTINGS = "Settings";
	public static final String MENU_QUANTUM = "Quantum Fluctuations";
	public static final String MENU_RESET = "Reset";
	public static final String MENU_HELP = "Help";
	public static final String MENU_ABOUT = "About";
	
	public static final String ICON = "res/icon.png";
}
