package com.talixa.conway.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class ConwayPanel extends JPanel {
	
	private static final Random r = new Random(System.currentTimeMillis());
	
	private static final int CELL_SIZE = 10;	// cell size in pixels
	private static final int LIFETIME = 1000;	// life of each cell generation in ms
	private static final int BOARD_SIZE = 50;	// board length/width in cells
	
	private static boolean[][] cells;			// game board
	private static boolean[][] nextGen;			// next generation of board
	
	private static final Color EMPTY = Color.black;
	private static final Color LIFE = Color.green;
	
	public ConwayPanel() {	
		// set board to default state
		reset();
		
		// allow user to flip cells
		this.addMouseListener(new MouseListener() {			
			public void mouseReleased(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}			
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {		
				// get grid coordinates
				int x = e.getX() / CELL_SIZE;
				int y = e.getY() / CELL_SIZE;
				
				if (x >= 0 && x < BOARD_SIZE && y >= 0 && y < BOARD_SIZE) {
					cells[x][y] = !cells[x][y];
					nextGen[x][y] = cells[x][y];
					invalidate();
					repaint();
				}
			}
		});
	}
	
	/**
	 * Reset the board to the default state with 4 small exploders and a spinner
	 */
	public void reset() {
		cells = new boolean[BOARD_SIZE][BOARD_SIZE];
		nextGen = new boolean[BOARD_SIZE][BOARD_SIZE];
	
		// small exploder
		cells[10][10] = true;
		cells[11][9] = true;
		cells[11][10] = true;
		cells[11][11] = true;
		cells[12][9] = true;
		cells[12][11] = true;
		cells[13][10] = true;
		
		// small exploder
		cells[40][40] = true;
		cells[41][39] = true;
		cells[41][40] = true;
		cells[41][41] = true;
		cells[42][39] = true;
		cells[42][41] = true;
		cells[43][40] = true;
		
		// small exploder
		cells[10][40] = true;
		cells[11][39] = true;
		cells[11][40] = true;
		cells[11][41] = true;
		cells[12][39] = true;
		cells[12][41] = true;
		cells[13][40] = true;
		
		// small exploder
		cells[40][10] = true;
		cells[41][9] = true;
		cells[41][10] = true;
		cells[41][11] = true;
		cells[42][9] = true;
		cells[42][11] = true;
		cells[43][10] = true;
		
		// spinner
		cells[25][24] = true;
		cells[25][25] = true;
		cells[25][26] = true;
	}
	
	/**
	 * Start the animation
	 */
	public void start() {
		Timer animation = new Timer(LIFETIME, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				repaint();
				iterate();
			}
		});
		
		animation.start();
	}
	
	// Create next generation
	// Rules: 
	// 1) Any live cell with fewer than two live neighbors dies (referred to as underpopulation or exposure).
	// 2) Any live cell with more than three live neighbors dies (referred to as overpopulation or overcrowding).
	// 3) Any live cell with two or three live neighbors lives, unchanged, to the next generation.
	// 4) Any dead cell with exactly three live neighbors will come to life.
	private void iterate() {
		for(int i = 0; i < cells.length; ++i) {
			for (int j = 0; j < cells.length; ++j) {
				int neighbors = getNeighborCount(i,j);
				boolean live = cells[i][j];
				
				// cell will be alive if either rule 3 or 4 
				if (!live && neighbors == 3) {
					nextGen[i][j] = true;
				} else if (live && (neighbors == 2 || neighbors == 3)) {
					nextGen[i][j] = true;
				} else {
					nextGen[i][j] = false;
				}
				
				// for fun, add quantum fluctuations to the life simulation
				if (quantum) {
					if (r.nextFloat() < QUANTUM_CHANCE) {
						nextGen[i][j] = !nextGen[i][j];
					}
				}
			}
		}
		
		// flip scratch & cells
		boolean[][] tmp = cells;
		cells = nextGen;
		nextGen = tmp;
	}
	
	private int getNeighborCount(int row, int col) {
		int count = 0;
		
		for(int i = row - 1; i <= row + 1; ++i) {
			for (int j = col -1; j <= col + 1; ++j) {
				// ignore self
				if (i == row && j == col) {
					continue;
				}
				// don't go outside of board
				if (i < 0 || j < 0 || i == BOARD_SIZE || j == BOARD_SIZE) {
					continue;
				}
				if (cells[i][j]) {
					++count;
				}
			}
		}
		return count;
	}
	
	private boolean quantum = false;
	private static final float QUANTUM_CHANCE = 1f/900f;
	
	/**
	 * Set state of quantum fluctuations.
	 * If this is enabled, cells will randomly change state in
	 * addition to the standard rules of Conway's Game of Life
	 * 
	 * @param enabled desired state of quantum fluctuations
	 */
	public void setQuantum(boolean enabled) {
		quantum = enabled;
	}
	
	@Override
	public void paint(Graphics g) {		
		super.paint(g);
		
		// fill space black
		g.setColor(EMPTY);
		g.fillRect(0, 0, CELL_SIZE * cells.length, CELL_SIZE * cells.length);
		
		// fill live cells
		for(int i = 0; i < cells.length; ++i) {
			for(int j = 0; j < cells.length; ++j) {
				if (cells[i][j]) {
					g.setColor(LIFE);
					g.fillRect(i*CELL_SIZE, j*CELL_SIZE, CELL_SIZE, CELL_SIZE);
				}
			}
		}
	}
}
