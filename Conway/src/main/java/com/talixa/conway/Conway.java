package com.talixa.conway;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import com.talixa.conway.frames.FrameAbout;
import com.talixa.conway.listeners.DefaultWindowListener;
import com.talixa.conway.listeners.ExitActionListener;
import com.talixa.conway.shared.ConwayConstants;
import com.talixa.conway.shared.IconHelper;
import com.talixa.conway.widgets.ConwayPanel;

public class Conway {

	private static JFrame frame;
	private static ConwayPanel conway;
	
	private static void createAndShowGUI() {
		frame = new JFrame(ConwayConstants.TITLE_MAIN);
		
		// set close functionality 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		frame.addWindowListener(new DefaultWindowListener());
		
		conway = new ConwayPanel();
		frame.add(conway);
		
		// set icon
		IconHelper.setIcon(frame);	
													
		// add menus
		addMenus();			
		
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(ConwayConstants.APP_WIDTH,ConwayConstants.APP_HEIGHT));
		int left = (screenSize.width/2) - (ConwayConstants.APP_WIDTH/2);
		int top  = (screenSize.height/2) - (ConwayConstants.APP_HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);
		
		// start animation
		conway.start();
	}
			
	private static void addMenus() {
		// Setup file menu
		JMenu fileMenu = new JMenu(ConwayConstants.MENU_FILE);
		fileMenu.setMnemonic(KeyEvent.VK_F);			
			
		JMenuItem exitMenuItem = new JMenuItem(ConwayConstants.MENU_EXIT);
		exitMenuItem.setMnemonic(KeyEvent.VK_X);
		exitMenuItem.addActionListener(new ExitActionListener(frame));
		fileMenu.add(exitMenuItem);	
		
		//*******************************************************************************
		// Settings menu
		JMenu settings = new JMenu(ConwayConstants.MENU_SETTINGS);
		settings.setMnemonic(KeyEvent.VK_S);

		JMenuItem reset = new JMenuItem(ConwayConstants.MENU_RESET);
		reset.setMnemonic(KeyEvent.VK_R);
		reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				conway.reset();			
			}						
		});
		settings.add(reset);
		
		JCheckBoxMenuItem quantumEnabled = new JCheckBoxMenuItem(ConwayConstants.MENU_QUANTUM);
		quantumEnabled.setMnemonic(KeyEvent.VK_Q);
		quantumEnabled.setSelected(false);
		quantumEnabled.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				conway.setQuantum(quantumEnabled.isSelected());			
			}						
		});
		settings.add(quantumEnabled);
		
		
		
		//*******************************************************************************
		// Setup help menu
		JMenu helpMenu = new JMenu(ConwayConstants.MENU_HELP);
		helpMenu.setMnemonic(KeyEvent.VK_H);
		JMenuItem aboutMenuItem = new JMenuItem(ConwayConstants.MENU_ABOUT);
		aboutMenuItem.setMnemonic(KeyEvent.VK_A);
		aboutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrameAbout.createAndShowGUI(frame);				
			}						
		});
		helpMenu.add(aboutMenuItem);

		//*******************************************************************************
		// Add menus to menubar
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);	
		menuBar.add(settings);
		menuBar.add(helpMenu);
		frame.setJMenuBar(menuBar);
	}
		
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {			
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
